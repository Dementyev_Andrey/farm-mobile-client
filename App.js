import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { HomeScreen } from './src/screens/HomeScreen';
import { FastOrderScreen } from './src/screens/FastOrderScreen';
import { ProductCard } from './src/screens/ProductCart';


const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        />
      <Stack.Screen
        name="FastOrder"
        component={FastOrderScreen}
        />
      <Stack.Screen
        name="ProductCard"
        component={ProductCard}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
