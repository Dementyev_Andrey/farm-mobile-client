import React, {useState, useEffect} from 'react';
import {View, SafeAreaView, StyleSheet, TouchableOpacity} from 'react-native';
import {HomePageHeader} from '../components/HomePageHeader'
import {SearchField} from '../components/SearchField'
import {Btn} from '../components/Btn'
import {FooterMenue} from '../sets/FooterMenue'


export const HomeScreen =  ({navigation}) => {
    let url1 = 'http://10.0.2.2:8000/api-mobile/product/7yss1p8tsu99/'
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
  
    const getMovies = async (url) => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        setData([json]);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    }
  
    useEffect(() => {
      getMovies(url1);
    }, []);

    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
              <HomePageHeader />
              <View style={styles.bodyContainer}>
                <SearchField inpFieldPlaceholder={'Введите название препарата или компонента'} />
                <TouchableOpacity 
                    onPress={ () => {navigation.navigate('FastOrder')}} style={{height: '5%'}}>
                    <Btn title='Быстрый заказ' />
                </TouchableOpacity>
              </View>  
              <FooterMenue/>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
container: {
    height: '98%',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
},
bodyContainer: {
  height: '80%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}
});