import React from 'react';
import {View, SafeAreaView, StyleSheet, TouchableOpacity, StatusBar, Text} from 'react-native';
import {InputField} from '../components/InputField'
import {Btn} from '../components/Btn'
import {FooterMenue} from '../sets/FooterMenue'


export const FastOrderScreen = ({navigation}) => {
    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
                <View>
                    <Text style={styles.titleText}>Быстрое оформление заказа </Text>
                    <StatusBar backgroundColor="#ff5204" /> 
                    <Text>Ваше имя</Text>
                    <InputField inpFieldPlaceholder={'ФИО'} fieldHeight={'8%'} />
                    <Text>Ваш контактный номер телефона</Text>
                    <InputField inpFieldPlaceholder={'+7(___)___-__-__'} fieldHeight={'8%'} />
                    <Text>Ваш заказ</Text>
                    <InputField inpFieldPlaceholder={'Какие препрепараты необходимы'} fieldHeight={'40%'} />
                    <TouchableOpacity style={ {height: "10%"} } onPress={ () => {navigation.navigate('Home')} }>
                        <Btn title='Отправить заявку' />
                    </TouchableOpacity>
                </View>
                <FooterMenue />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
container: {
    height: '98%',
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
},
titleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
    marginBottom: '5%'
}
});