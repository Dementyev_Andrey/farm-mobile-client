import React from 'react';
import {View, SafeAreaView, StyleSheet, Text, Image, ScrollView} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { FooterMenue } from '../sets/FooterMenue';


export const ProductCard = ({route, navigation}) => {

  const {data} = route.params
  // console.log(`https://ugmk-farm.ru${data.image}`)

  return (
  //   <SafeAreaView style={styles.container}>
  //   <ScrollView style={styles.scrollView}>
  //     <Text style={styles.text}>
  //       Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
  //       eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
  //       minim veniam, quis nostrud exercitation ullamco laboris nisi ut
  //       aliquip ex ea commodo consequat. Duis aute irure dolor in
  //       reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
  //       pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
  //       culpa qui officia deserunt mollit anim id est laborum.
  //     </Text>
  //   </ScrollView>
  // </SafeAreaView>
    
    <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <Text style={{fontSize: 22, fontWeight: 'bold', color: '#000', marginBottom: '5%'}}>{data.title}</Text>
            <Image 
                source={{uri: `https://ugmk-farm.ru${data.image}`}}
                style={{width: '50%', height:'50%', marginBottom: '5%'}}
                ></Image> 
          <ScrollView style={styles.scrollView}>
            <Text style={{fontSize: 18, color: '#000'}}>Производитель: {data.manufacturer}</Text>
            <Text style={{fontSize: 18, color: '#000'}}>Производитель: {data.manufacturer_country}</Text>
          </ScrollView> 
          <FooterMenue  style={{bottom: 10}}/>
        </View>
    </SafeAreaView>
    
  );
};



const styles = StyleSheet.create({
    container: {
        height: '98%',
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    scrollView: {
      width: '100%'
    }
});
