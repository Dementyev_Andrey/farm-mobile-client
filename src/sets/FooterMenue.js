import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';

export const FooterMenue = () => {
  const navigation = useNavigation()
  return (
    <View style={styles.footerMenue}>
      <TouchableOpacity onPress={ () => { navigation.navigate('Home')}} style={styles.menueElem}>
        <View style={[ {display: 'flex', justifyContent: 'center', alignItems: 'center'}]}>
          <Image source={require('../assets/images/home_icon_black.png')}/>
        </View>
      </TouchableOpacity>
      <View style={styles.menueElem} />
      <View style={styles.menueElem} />
      <View style={styles.menueElem} />
      <View style={styles.menueElem} />
    </View>
)};

const styles = StyleSheet.create({
  footerMenue: {
    marginTop: '5%',
    width: '100%',
    height: '6%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f2f2f2',
  },
  menueElem: {
    width: '10%',
    height: '90%',
    borderRadius: 8,
    borderColor: '#000',
    borderWidth: 1
  }
});
