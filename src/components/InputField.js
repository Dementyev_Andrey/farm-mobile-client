import React from 'react';
import {StyleSheet, TextInput, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const InputField = (props) => {
return (
    <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['hsl(0,0%,85%)', 'white', 'white', 'white', 'white']}
        style={[(styles.linearGradient, styles.searchField, {height: props.fieldHeight, borderRadius: 7, }) ]}>
        <TextInput
            style={styles.searchInput}
            placeholder={props.inpFieldPlaceholder}
        />
    </LinearGradient>
  );
};
var styles = StyleSheet.create({

  searchInput: {
    fontSize: 14,
    fontFamily: 'Gill Sans',
    textAlign: 'left',
    color: '#000',
  },
  searchField: {
    borderRadius: 7,
    marginTop: 0,
    marginBottom: 10,
    borderColor: 'hsl(0,0%,85%)',
    borderWidth: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    
  },
});
