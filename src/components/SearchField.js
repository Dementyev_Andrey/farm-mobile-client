import React, {useState, useEffect} from 'react';
import {StyleSheet, TextInput, Image, View, ScrollView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {SearchCard} from './SearchCard'

export const SearchField = ({inpFieldPlaceholder}) => {
  const [responseSearch, setResponseSearch] = useState([])
 
  const getSearchResponse = async (text) => {
    {
      try {
        const response = await fetch(`http://10.0.2.2:8000/api/market/search/${text}`);
        const json = await response.json() ;
        setResponseSearch(json.data)
        console.log(responseSearch)
      } catch (error) {
        console.error(error);
      }
    }
  }
  useEffect(() => { 
      getSearchResponse();
  }, []);
return (
  <View>
    <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['hsl(0,0%,85%)', 'white', 'white', 'white', 'white']}
        style={(styles.linearGradient, styles.searchField)}>
        <TextInput
            style={styles.searchInput}
            placeholder={inpFieldPlaceholder}
            onChangeText={text => {
              if (text.length > 2) {
                getSearchResponse(text);
              } else return;
            }} 
            />
        <Image source={require('../assets/images/search.png')} style={styles.icon} />
    </LinearGradient>
    <ScrollView style={{height: '85%'}}>
      <View style={{height: '100%'}}>
        {
          responseSearch.map(elem => { if (elem.title)
            return ( 
              <SearchCard key={elem.id} product={elem} />
            )
          }
          )
        }
      </View>
    </ScrollView>
  </View>
  );
};
var styles = StyleSheet.create({

  searchInput: {
    padding: '2%',
    fontSize: 14,
    fontFamily: 'Gill Sans',
    textAlign: 'left',
    color: '#000',
  },
  searchField: {
    borderRadius: 7,
    marginTop: 0,
    marginBottom: 10,
    borderColor: 'hsl(0,0%,85%)',
    borderWidth: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    marginTop: 10,
    marginRight: 10,
    marginBottom: 5,
    height: 25,
    width: 25,
  },

});
