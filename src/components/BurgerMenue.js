import React from 'react';
import {View, StyleSheet} from 'react-native';

export const BurgerMenue = () => {
	return (
    <View style={styles.burgerMenue}>
			<View style={styles.block1}></View>
			<View style={styles.block1}></View>
			<View style={styles.block1}></View>
    </View>
  );
};

const styles = StyleSheet.create({
	burgerMenue: {
		marginTop: 10,
		height: 30,
		width: 40,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-around',
		alignItems: 'flex-start',
	},
	block1: {
    height: 3,
		width: '80%',
		backgroundColor: 'black',
	},
});
