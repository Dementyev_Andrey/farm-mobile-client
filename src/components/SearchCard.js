import React from 'react';
import {View, StyleSheet, Image, Text, FlatList, TextInput, ScrollView} from 'react-native';
import { ProductSmall } from './ProductSmall';

export const SearchCard = (props) => {

  return (
    <ScrollView horizontal={false} >
        <View style={styles.searchCard}> 
            <Text>Наименование: {props.product.title}</Text>
            <Text>ID продукта {props.product.id}</Text>
        </View>
  </ScrollView>
)};

const styles = StyleSheet.create({
  searchCard: {
    width: '100%',
    height: '100%',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5, 
    flexDirection: 'column'
  }
});

