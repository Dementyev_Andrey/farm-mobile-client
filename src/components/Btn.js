import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
let a

export const Btn = (props) => {
return (
    <View style={[styles.fastOrder]}>
      <Text style={styles.text}>{props.title}</Text>
    </View>
    );
};

const styles = StyleSheet.create({
  fastOrder: {
    marginTop: '2%',
    backgroundColor: '#ff5204',
    height: '100%',
    borderRadius: 6,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 20,
  },
});