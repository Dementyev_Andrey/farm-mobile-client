import React from 'react';
import {View, StyleSheet, Image, Text, FlatList} from 'react-native';
import { ProductSmall } from './ProductSmall';

export const HomePageBody = ({data}) => {
  // console.log('HPB', data)
  const products = [
      ];
  return (

    <View style={styles.container1}>
        {data.map(elem => <ProductSmall key={elem["id"]} product={elem} screenName={'ProductCard'} /> )}
    </View>

    // <View style={styles.container1}>
    //   <FlatList 
    //     keyExtractor ={item => item.id}
    //     data={products}
    //     horizontal={false}
    //     numColumns={2}
    //     renderItem={({ item }) => <ProductSmall product={item}> </ProductSmall>}
    //     />
    //   </View>
)};

const styles = StyleSheet.create({
  container1: {
    height: '65%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  body: {
    height: '10%',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
});

