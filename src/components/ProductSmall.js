import React from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import { ProductCard } from '../screens/ProductCart';
import { useNavigation } from '@react-navigation/native';

export const ProductSmall = (props) => {
    const navigation = useNavigation()
    return (
    <TouchableOpacity 
    onPress={ () => {navigation.navigate(props.screenName, {data: props.product})}}
    style={styles.card}>
        <Text>{props.product["title"]}</Text>
        <Image 
            source={{uri: `https://ugmk-farm.ru${props.product["image"]}`}}
            style={{width: '50%', height: '100%'}} />
        <Text> ID: {props.product["id"]}</Text>
        <Text>Производитель: {props.product["manufacturer"]}</Text>
    </TouchableOpacity>
)};

const styles = StyleSheet.create({
    product: {
        width: '45%',
        height: '80%',
        marginLeft: '1%',
        marginRight: '1%',
        borderRadius: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    card: {
        height: '40%',
        width: '50%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
});
