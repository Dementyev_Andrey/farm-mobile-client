import React from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';

export const Cart = () => {
return (
    <View style={styles.cart}>
        <Image source={require('../assets/images/cart-icon.png')} style={styles.cartIcon} />
        <View style={styles.counterArea}>
            <Text style={styles.counter}>5</Text>
        </View>
    </View>
    );
};

const styles = StyleSheet.create({
cart: {
    height: 50,
    width: 50,
    backgroundColor: '#ff5204',
    borderRadius: 50,
    zIndex: 0,
},
cartIcon: {
    left: '25%',
    top: '40%',
    height: '40%',
    width: '40%',
    zIndex: 2,
},
counterArea: {
    backgroundColor: '#f2f2f2',
    zIndex: 1,
    height: '60%',
    width: '60%',
    borderRadius: 50,
    bottom: 30,
    left: 25,
},
counter: {
    color: '#ff5204',
    top: -2,
    left: 5,
    fontSize: 20,
    fontWeight: 'bold',
},
});