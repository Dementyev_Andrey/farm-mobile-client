import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, StatusBar} from 'react-native';
import {BurgerMenue} from './BurgerMenue'
import {Cart} from './Cart'

export const HomePageHeader = (navigation) => {

  return (
    <View style={{height: '8%', marginTop: 10 }}>
        <StatusBar backgroundColor="#ff5204" />
        <View style={styles.upperMenu}>
          <TouchableOpacity >
              <BurgerMenue /> 
          </TouchableOpacity>
          <Image source={require('../assets/images/logo-farm.png')} />
          <TouchableOpacity>
            <Cart />
          </TouchableOpacity>
        </View>
    </View>
)};

const styles = StyleSheet.create({
  upperMenu: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
